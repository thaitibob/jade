# Projet JADE

* Drui Dimitri
* Melchior Clément
* Valle Jonathan

### Docker

Installation de docker (vous en avez besoin pour la BDD)

```
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker
```

Installation de docker-compose

```
sudo apt install docker-compose
```

## Lancement du Projet

Il faut télécharger jade et ajouter jade.jar au classpath

### Service de stockage

Il faut tout d'abord lancer les services de base de données.
Pensez à vérifier que votre port 3309

```
docker-compose up -d
```

Connexion à la BDD MariaDB :
```
# identifiant : root
# mot de passe : admin
http://localhost:8081/
```
