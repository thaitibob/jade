package behaviors;

import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import core.entity.Artwork;
import core.entity.DemandeArtworks;
import core.entity.Proposition;
import core.statut.DemandeStatut;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*

Réponse:
{"replyProposal": true}
 */

public class AcceptationBehavior extends CyclicBehaviour {

	private static final Logger LOGGER = Logger.getLogger(AcceptationBehavior.class.getName());

	private Connection connection = new MariaDB();

	@Override
	public void action() {
		MessageTemplate mt1 = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
        ACLMessage acceptation = myAgent.receive(mt1);
        
        if (acceptation != null) {
        	try {
				String sender = acceptation.getSender().getName();
				String uuid = acceptation.getConversationId();

				LOGGER.log(Level.INFO, "Demande d'acceptation reçue, sender=" + sender);

				ArrayList<DemandeArtworks> at = new ArrayList<>();
				at = connection.findDemande(uuid);

				if(checkExcluArtworks(at)){
					double prix = connection.getPropositionPrice(new Proposition(uuid, DemandeStatut.EN_COURS.name()));
					connection.updateProposition(new Proposition(uuid, DemandeStatut.ACCEPT.name()));
					connection.updateStatutDemande(uuid, DemandeStatut.ACCEPT.name());

					for (DemandeArtworks a : at){
						int nb_vente = a.getVENDU();
						nb_vente++;
						int id = a.getID_ARTWORK();
						connection.updateArtworkVendu(id,nb_vente,a.isREQ_EXCLU());
					}

					double c = connection.findCash();
					double somme = c+prix;
					connection.updateCash(somme);

					sendMsg(acceptation, uuid);
				}else {
					// La demande n'est plus valable
					connection.updateStatutDemande(uuid, DemandeStatut.VENTE_INTERROMPUE.name());
					connection.updateProposition(new Proposition(uuid, DemandeStatut.VENTE_INTERROMPUE.name()));
					throw new Exception("Problème sur un oeuvre : l'oeuvre a été attribué comme exclusive");
				}
			}catch (Exception e){
				sendMsgError(acceptation);
				LOGGER.log(Level.SEVERE, "Envoie d'un message d'erreur aux distributeur");
			}
        }else {
			LOGGER.log(Level.INFO, "AcceptationBehaviour en arrière plan");
			this.block();
		}
	}

	/**
	 * On vérifie qu'une oeuvre n'a pas été attribuée entre la demande d'achat et la négociation
	 * @param at
	 * @return
	 */
	private boolean checkExcluArtworks(ArrayList<DemandeArtworks> at){
		for(int i = 0; i<at.size(); i++) {
			int id_artwork = at.get(i).getID_ARTWORK();
			Artwork a = connection.findArtworkByIdFilm(id_artwork);
			// Dans la demande, le distributeur demande l'exclusivité mais on a deja vendu des oeuvres
			if(at.get(i).isREQ_EXCLU() && a.getVendu()>0){
				return false;
			}
			// Si l'oeuvre demandé est déjà exclusive, on refuse
			if(a.isExclu()){
				return false;
			}
			// verifie si on a bien produit l'oeuvre
			if(!a.isProduit()){
				return false;
			}
		}
		return true;
	}

	/**
	 * Envoie un message de réponse à l'acceptation
	 * @param acceptation
	 * @param uuid
	 */
	private void sendMsg(ACLMessage acceptation, String uuid) {
		try {
			ACLMessage replyMessage = acceptation.createReply();
			replyMessage.setConversationId(uuid);
			replyMessage.setContent("{\"replyProposal\":\"true\"}");
			replyMessage.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
			myAgent.send(replyMessage);
			LOGGER.log(Level.INFO, "Réponse de la négociation envoyée !");
		}catch (Exception e){
			LOGGER.log(Level.SEVERE, "Problème envoie de la réponse de demande d'achat envoyée !");
		}
	}

	private void sendMsgError(ACLMessage demande_achat) {
		try {
			ACLMessage replyMessage = demande_achat.createReply();
			replyMessage.setContent("{\"failure\":\"Problème dans la requête\"}");
			replyMessage.setPerformative(ACLMessage.FAILURE);
			myAgent.send(replyMessage);
			LOGGER.log(Level.INFO, "Message d'erreur envoyé au distributeur !");
		}catch (Exception e){
			LOGGER.log(Level.SEVERE, "Problème lors de l'envoie du message d'erreur au distributeur !");
		}
	}

}
