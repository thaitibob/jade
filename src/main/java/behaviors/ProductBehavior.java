package behaviors;

import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import core.entity.Artwork;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/*
 *
 *
{
    type_sujet: string,
    id_sujet: string,
}




 */

public class ProductBehavior extends TickerBehaviour {

    private ArrayList<Artwork> catalog = new ArrayList<Artwork>(); //contient les artworks recuperes via la BDD avec prix
    private AID aid_e_reputation=new AID("rma@192.168.1.20:1234/JADE");
    private Connection co = new MariaDB();
    private double cash;

    public ProductBehavior(Agent a, long period) {
        super(a, period);
    }

    @Override
    protected void onTick() {
        this.initCatalog();
        ArrayList<Artwork> catalogSort = catalog;

        Collections.sort(catalogSort, new Comparator<Artwork>() {
            @Override
            public int compare(Artwork o1, Artwork o2) {
                return Double.compare(o1.getPrice(), o2.getPrice());
            }
        });
        this.cash = co.findCash();
        double alea = Math.random();
        if (cash>1 && this.cash>50) {
            if (alea < 0.5) {
                produceHigh(catalogSort);
            } else if (alea < 0.8) {
                produceMedium(catalogSort);
            } else{
                produceLow(catalogSort, alea);
            }
        } else {
            System.out.println("Pas assez de cash");
        }
        System.out.println("Le cash disponible : "+this.cash);
    }


    private void produceHigh(ArrayList<Artwork> artworks) {
        if (artworks.size()!=0) {
            this.co.updateCash(this.cash-(artworks.get(0).getPrice()*0.05));
            artworks.get(0).setProduit(true);
            co.updateArtwork(artworks.get(0));
            System.out.println("L'oeuvre " + artworks.get(0).getIdFilm() + " : " + artworks.get(0).getTitre() + " a été produite");
        }
    }

    private void produceMedium(ArrayList<Artwork> artworks) {
        if (artworks.size()!=0) {
            int alea = (int) Math.random()*(artworks.size()-1);
            this.co.updateCash(this.cash-(artworks.get(alea).getPrice()*0.05));
            artworks.get(alea).setProduit(true);
            co.updateArtwork(artworks.get(alea));
            System.out.println("L'oeuvre " + artworks.get(alea).getIdFilm() + " : " + artworks.get(alea).getTitre() + " a été produite");
        }
    }
    private void produceLow(ArrayList<Artwork> artworks, double alea) {
        if (artworks.size()!=0) {
            this.co.updateCash(this.cash-(artworks.get(artworks.size()-1).getPrice()*0.05));
            artworks.get(artworks.size()-1).setProduit(true);
            co.updateArtwork(artworks.get(artworks.size()-1));
            System.out.println("L'oeuvre " + artworks.get(artworks.size()-1).getIdFilm() + " : " + artworks.get(artworks.size()-1).getTitre() + " a été produite");
        }
    }

    // {"listArtwork":[{"idArtwork":"1258","excluArtwork":"false"},{"idArtwork":"858","excluArtwork":"false"}]}
    // {"listArtwork":[{"idArtwork":"1258","excluArtwork":"false"}]}
    // {"listArtwork":[{"idArtwork":"1","excluArtwork":"false"}]}
    // {"listArtwork":[{"idArtwork":"1","excluArtwork":"false"},{"idArtwork":"858","excluArtwork":"false"}]}
    // json string pour msg de test

    //envoie une reponse au format request si il trouve pas l'oeuvre
    private void sendMsgErreur(AID aid_distributor) {
        AMSAgentDescription [] agents = null;
        ACLMessage answer = new ACLMessage(ACLMessage.PROPOSE);
        answer.setContent("Artworks non trouves");
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults ( new Long(-1) );
            agents = AMSService.search( this.getAgent(), new AMSAgentDescription (), c );
            for (int i=0; i<agents.length;i++) {
                if(agents[i].getName().equals(aid_distributor)) {
                    System.out.println("Msg Erreur envoyee au distributeur "+agents[i].getName()+", "+answer.getContent());
                    answer.addReceiver(aid_distributor);
                    myAgent.send(answer);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    // envoie une requete au format REQUEST a e-reputation
    private void sendMsg(Artwork artwork) {
        AMSAgentDescription [] agents = null;
        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.setContent("{\"popularite\":{\"type_sujet\":\"oeuvre\",\"id_sujet\":\""+artwork.getIdFilm()+"\"}}");
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults ( new Long(-1) );
            agents = AMSService.search( this.getAgent(), new AMSAgentDescription (), c );
            for (int i=0; i<agents.length;i++) {
                if(agents[i].getName().equals(aid_e_reputation)) {
                    //System.out.println("Proposition envoyé a e-reputation "+agents[i].getName()+", "+request.getContent());
                    request.addReceiver(aid_e_reputation);
                    myAgent.send(request);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initCatalog() {
        this.catalog = co.findAllArtworkNotProduced();
    }

}