package behaviors;

import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import core.entity.*;
import core.ontologie.DemandeAchat;
import core.statut.DemandeStatut;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;

/*

Requete REQUEST:
{"listArtwork":[{"idArtwork":"1","excluArtwork":false},{"idArtwork":"2","excluArtwork":true}]}

Reponse PROPOSE:
{"priceProposal": 15.0}
 */

public class DemandeAchatBehavior extends CyclicBehaviour {

	private static final Logger LOGGER = Logger.getLogger(DemandeAchatBehavior.class.getName());
	private ArrayList<Artwork> catalog = new ArrayList<Artwork>();

	private Connection connection = new MariaDB();

	@Override
	public void action() {
		MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
		ACLMessage demande_achat = myAgent.receive(mt);

		if (demande_achat != null) {
			try {
				if(isCorrect(demande_achat)){
					String content = demande_achat.getContent();
					String sender = demande_achat.getSender().getName();

					LOGGER.log(Level.INFO, "Demande d'achat reçue, content=" + content + "; sender=" + sender);

					String uuid = UUID.randomUUID().toString();

					Gson gson = new Gson();
					DemandeAchat demande = gson.fromJson(content, new TypeToken<DemandeAchat>(){}.getType());

					//check si les id existent sinon message d'erreur
					if(checkExistsArtworks(demande, sender)){
						//insertion de la demande & insertion de requested artworks
						connection.insertDemande(new Demande(uuid,sender));
						for(int i = 0; i<demande.getlistArtwork().size(); i++) {
							int id_artwork = demande.getlistArtwork().get(i).getIdArtwork();
							boolean exclu = demande.getlistArtwork().get(i).isExcluArtwork();
							connection.insertRequested_artworks(new RequestedArtworks(uuid,id_artwork,exclu));
						}

						ArrayList<DemandeArtworks> at = new ArrayList<>();
						at = connection.findDemande(uuid);
						double prix = getPriceDemandeArtworksForList(at);

						// Ajout de la proposition
						connection.insertProposition(new Proposition(uuid, prix, DemandeStatut.EN_COURS.name()));

						sendMsg(prix,demande_achat,uuid);
					}else {
						throw new Exception("Problème sur un oeuvre : l'oeuvre n'existe pas, est exclusive ou ne peut-être demandée comme exclusive");
					}
				}else{
					throw new Exception("Problème lors du parse de la proposition d'achat");
				}
			}catch (Exception e){
				sendMsgError(demande_achat);
				LOGGER.log(Level.SEVERE, "Envoie d'un message d'erreur aux distributeur");
			}
		} else {
			LOGGER.log(Level.INFO, "DemandeAchatBehaviour en arrière plan");
			this.block();
		}
	}

	/**
	 * Envoie un message message d'erreur au distributeur
	 * @param demande_achat
	 */
	private void sendMsgError(ACLMessage demande_achat) {
		try {
			ACLMessage replyMessage = demande_achat.createReply();
			replyMessage.setContent("{\"failure\":\"Oeuvre introuvable ou problème dans la requête\"}");
			replyMessage.setPerformative(ACLMessage.FAILURE);
			myAgent.send(replyMessage);
			LOGGER.log(Level.INFO, "Message d'erreur envoyé au distributeur !");
		}catch (Exception e){
			LOGGER.log(Level.SEVERE, "Problème lors de l'envoie du message d'erreur au distributeur !");
		}
	}

	/**
	 * Envoie un message de réponse à l'offre du distributeur
	 * @param prix
	 * @param demande_achat
	 * @param uuid
	 */
	private void sendMsg(double prix, ACLMessage demande_achat, String uuid) {
		try {
			ACLMessage replyMessage = demande_achat.createReply();
			replyMessage.setConversationId(uuid);
			replyMessage.setContent("{\"priceProposal\":"+prix+"}");
			replyMessage.setPerformative(ACLMessage.PROPOSE);
			myAgent.send(replyMessage);
			LOGGER.log(Level.INFO, "Réponse demande d'achat envoyée !");
		}catch (Exception e){
			LOGGER.log(Level.SEVERE, "Problème envoie de la réponse de demande d'achat envoyée !");
		}
	}

	/**
	 * Donne le prix pour une liste d'artwork
	 * @param listDemandeArtworks
	 * @return
	 */
	private Double getPriceDemandeArtworksForList (ArrayList<DemandeArtworks> listDemandeArtworks) {
		double price=0.0;
		for (DemandeArtworks da : listDemandeArtworks) {
			price = price + getPriceDemandeArtworksForUnique(da);
		}
		return price;
	}

	/**
	 * Parcours des oeuvres pour voir si elles existent en BDD
	 * Verification si demande d'oeuvre exclue alors nb vente = 0
	 * @param demande
	 * @return
	 */
	private boolean checkExistsArtworks(DemandeAchat demande, String sender){
		for(int i = 0; i<demande.getlistArtwork().size(); i++) {
			int id_artwork = demande.getlistArtwork().get(i).getIdArtwork();
			Artwork a = connection.findArtworkByIdFilm(id_artwork);
			if (a==null){
				return false;
			}else{
				// Dans la demande, le distributeur demande l'exclusivité mais on a deja vendu des oeuvres
				if(demande.getlistArtwork().get(i).isExcluArtwork() && a.getVendu()>0){
					return false;
				}
				// Si l'oeuvre demandé est déjà exclusive, on refuse
				if(a.isExclu()){
					return false;
				}
				// verifie si on a bien produit l'oeuvre
				if(!a.isProduit()){
					return false;
				}
				// verifie que le distributeur n'est pas déjà propriétaire
				if(!connection.DistributeurisNotOwner(sender,a.getIdFilm())){
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Test la validité du message JSON
	 * @param reponse
	 * @return
	 */
	private boolean isCorrect(ACLMessage reponse) {
		String schema_demande_achat = "{\"$id\":\"https://example.com/person.schema.json\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"description\":\"DemandeAchat\",\"type\":\"object\",\"properties\":{\"listArtwork\":{\"type\":\"array\",\"items\":{\"$ref\":\"#/definitions/DemandeArtwork\"}}},\"definitions\":{\"DemandeArtwork\":{\"type\":\"object\",\"required\":[\"idArtwork\",\"excluArtwork\"],\"properties\":{\"idArtwork\":{\"type\":\"string\",\"description\":\"identifiant\"},\"excluArtwork\":{\"type\":\"boolean\",\"description\":\"exclusivite\"}}}}}";
		return validateJsonDocument(schema_demande_achat,reponse.getContent());
	}

	public static boolean validateJsonDocument(String definition, String payload){
		try {
			JSONObject jsonSchema = new JSONObject(definition);
			JSONObject jsonData = new JSONObject(payload);
			Schema schema = SchemaLoader.load(jsonSchema);
			schema.validate(jsonData);
			return true;
		} catch(ValidationException ve) {
			System.out.println(readValidationException(ve));
			return false;
		}
	}

	public static String readValidationException(final ValidationException e)
	{
		final StringBuilder bld = new StringBuilder(e.getMessage());
		e.getCausingExceptions().stream().map(ValidationException::getMessage).forEach(err ->
		{
			bld.append(err);
			bld.append("\n");
		});
		return bld.toString();
	}


	private Double getPriceDemandeArtworksForUnique (DemandeArtworks da) {
		Artwork art = connection.findArtworkByIdFilm(da.getID_ARTWORK());

		if (da.isREQ_EXCLU()) {
			return art.getPrice();
		}
		return art.getPrice()*0.9;
	}

}