package behaviors;

import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import core.entity.Artwork;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 *
 *
{
    type_sujet: string,
    id_sujet: string,
}




 */

public class ReputationBehavior extends TickerBehaviour {

    private static final Logger LOGGER = Logger.getLogger(ReputationBehavior.class.getName());

    private ArrayList<Artwork> catalog = new ArrayList<Artwork>(); //contient les artworks recuperes via la BDD avec prix
    private AID aid_e_reputation=new AID("rma@192.168.1.20:1234/JADE");
    private Connection co = new MariaDB();

    public ReputationBehavior(Agent a, long period) {
        super(a, period);
    }

    @Override
    protected void onTick() {
        /*
        this.initCatalog();
        for (Artwork art : catalog) {
            sendMsg(art);
        }
         */
    }


    // {"listArtwork":[{"idArtwork":"1258","excluArtwork":"false"},{"idArtwork":"858","excluArtwork":"false"}]}
    // {"listArtwork":[{"idArtwork":"1258","excluArtwork":"false"}]}
    // {"listArtwork":[{"idArtwork":"1","excluArtwork":"false"}]}
    // {"listArtwork":[{"idArtwork":"1","excluArtwork":"false"},{"idArtwork":"858","excluArtwork":"false"}]}
    // json string pour msg de test

    //envoie une reponse au format request si il trouve pas l'oeuvre
    private void sendMsgErreur(AID aid_distributor) {
        AMSAgentDescription [] agents = null;
        ACLMessage answer = new ACLMessage(ACLMessage.PROPOSE);
        answer.setContent("Artworks non trouves");
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults ( new Long(-1) );
            agents = AMSService.search( this.getAgent(), new AMSAgentDescription (), c );
            for (int i=0; i<agents.length;i++) {
                if(agents[i].getName().equals(aid_distributor)) {
                    System.out.println("Msg Erreur envoyee au distributeur "+agents[i].getName()+", "+answer.getContent());
                    answer.addReceiver(aid_distributor);
                    myAgent.send(answer);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    // envoie une requete au format REQUEST a e-reputation
    private void sendMsg(Artwork artwork) {
        AMSAgentDescription [] agents = null;
        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.setContent("{\"popularite\":{\"type_sujet\":\"oeuvre\",\"id_sujet\":\""+artwork.getIdFilm()+"\"}}");
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults ( new Long(-1) );
            agents = AMSService.search( this.getAgent(), new AMSAgentDescription (), c );
            for (int i=0; i<agents.length;i++) {
                if(agents[i].getName().equals(aid_e_reputation)) {
                    //System.out.println("Proposition envoyé a e-reputation "+agents[i].getName()+", "+request.getContent());
                    request.addReceiver(aid_e_reputation);
                    myAgent.send(request);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initCatalog() {
        //this.catalog = co.findAllArtwork();
    }

}