package behaviors.inform;

import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import com.google.gson.Gson;

import core.ontologie.Popularity;
import jade.lang.acl.ACLMessage;

/*
 *
 *
Requete :
{
“popularite”:
{
    type_sujet: string,
    id_sujet: string,
}


Reponse :
{
    indice_popularite: int
}



 */

public class ReputationReceiverBehavior {

    private int prix_min=40;
    private int prix_max=100;
    private ACLMessage aclMessage;
    private Connection co = new MariaDB();

    public ReputationReceiverBehavior (ACLMessage aclMessage) {
        this.aclMessage=aclMessage;
    }

    public void action() {
        String content = this.aclMessage.getContent();
        String sender = this.aclMessage.getSender().getName();
        System.out.println("Reponse e-reputation recu, content=" + content + "; sender=" + sender);

        Gson gson = new Gson();
        Popularity pop = gson.fromJson(content, Popularity.class);
        double prix;
        prix = prix_max * pop.getPopularite() / 5.0;
        setPrice(pop.getOeuvre(),prix);
    }



    private void setPrice (int idOeuvre, double prix) {
        co.updateArtworkPrice(idOeuvre, prix);
        System.out.println("Prix maj");
    }

}