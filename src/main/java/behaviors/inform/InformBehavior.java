package behaviors.inform;

import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import org.json.JSONObject;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;

public class InformBehavior extends CyclicBehaviour {

    private Connection co = new MariaDB();

    @Override
    public void action() {
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
        ACLMessage reponse = myAgent.receive(mt);

        if (reponse != null) {
            CallAgent(reponse);
        } else {
            this.block();
        }
    }

    private void CallAgent(ACLMessage reponse) {
        if (isReputation(reponse)) {
            ReputationReceiverBehavior reputationReceiverBehavior = new ReputationReceiverBehavior(reponse);
            reputationReceiverBehavior.action();
        } else
            if (isSendingListArtwork(reponse)) {
                SendingListArtworksBehavior sendingListArtworksBehavior = new SendingListArtworksBehavior(reponse);
                this.getAgent().addBehaviour(sendingListArtworksBehavior);
        }
    }

    private boolean isReputation(ACLMessage reponse) {
        String schema_popularity ="{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"title\":\"Popularity\",\"description\":\"popularite\",\"type\":\"object\",\"properties\":{\"oeuvre\":{\"description\":\"oeuvre\",\"type\":\"string\"},\"popularite\":{\"description\":\"popularite\",\"type\":\"number\"}},\"required\":[\"oeuvre\",\"popularite\"]}";
        return validateJsonDocument(schema_popularity,reponse.getContent());
    }

    private boolean isSendingListArtwork(ACLMessage reponse) {
        String schema_IdArtworkArray = "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"title\":\"IdArtworkArray\",\"description\":\"IdArtworkArray\",\"type\":\"object\",\"properties\":{\"listArtwork\":{\"type\":\"array\",\"items\":{\"type\":\"string\"}}},\"required\":[\"listArtwork\"]}";
        return validateJsonDocument(schema_IdArtworkArray,reponse.getContent());
    }

    public static boolean validateJsonDocument(String definition, String payload){
        try {
            JSONObject jsonSchema = new JSONObject(definition);
            JSONObject jsonData = new JSONObject(payload);
            Schema schema = SchemaLoader.load(jsonSchema);
            schema.validate(jsonData);
            return true;
        } catch(ValidationException ve) {
            System.out.println(readValidationException(ve));
            return false;
        }
    }

    public static String readValidationException(final ValidationException e)
    {
        final StringBuilder bld = new StringBuilder(e.getMessage());
        e.getCausingExceptions().stream().map(ValidationException::getMessage).forEach(err ->
        {
            bld.append(err);
            bld.append("\n");
        });
        return bld.toString();
    }
}