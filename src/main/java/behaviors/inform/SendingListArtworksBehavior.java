package behaviors.inform;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import core.ontologie.ListIdArtwork;
import core.entity.Artwork;

import core.ontologie.DemandeArtwork;

import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;


/*
 * Requete:
{
    "listArtwork": []Long, (peut etre null, pour obtenir toutes les oeuvre dispo)
}
Reponse:
{
    "listArtwork": [(Liste des oeuvres dispo)
        {
            "idArtwork": Long,
           "excluArtwork": bool
        }
    ] 
}
les deux sont au format INFORM
 */

public class SendingListArtworksBehavior extends SimpleBehaviour {
	
	ArrayList<Long> requested_artworks_id = new ArrayList<Long>();
	ArrayList<Artwork> real_artworks = new ArrayList<Artwork>();
	private Connection co = new MariaDB();
	private ACLMessage aclMessage;

	public SendingListArtworksBehavior (ACLMessage aclMessage) {
		this.aclMessage=aclMessage;
	}

	public SendingListArtworksBehavior () {
	}

	@Override
	public void action() {
		 String sender = this.aclMessage.getSender().getName();
		 String content = this.aclMessage.getContent();
		 System.out.println("Demande d'infos reçue, sender=" + sender);
		 AID aid_dis = new AID(sender);
		 convertContentFromMsg(aid_dis, content);
	}

	@Override
	public boolean done() {
		return true;
	}

	// {"listArtwork":["1258","858"]}
	// {"listArtwork":null}
	
	private void convertContentFromMsg(AID aid_distributor, String json_content) {
		Gson gson = new Gson();
		ListIdArtwork ial = gson.fromJson(json_content, new TypeToken<ListIdArtwork>(){}.getType());
		if(ial.getlistArtwork()==null) {
			sendMsg(aid_distributor,getAllArtworks());
		} else {
			sendMsg(aid_distributor,getSomeArtworks(ial));
		}
	}

	private String getSomeArtworks(ListIdArtwork ial) {
		String result = "";
		initWithAllArtworks(); // on met tout pour tester apres
		ArrayList<DemandeArtwork> list_artwork = new ArrayList<DemandeArtwork>();
		for(int i=0; i<real_artworks.size(); i++) {
			int compare = real_artworks.get(i).getIdFilm();
			for(int j=0; j<ial.getlistArtwork().size(); j++) {
				if(compare==ial.getlistArtwork().get(j)) {
					int id = real_artworks.get(i).getIdFilm();
					boolean exclu = real_artworks.get(i).isExclu();
					list_artwork.add(new DemandeArtwork(id,exclu));
				}
			}
		}
		for(int k=0; k<list_artwork.size(); k++) {
			result += list_artwork.get(k).toJsonString();
		}
		return result;
	}
	
	private String getAllArtworks() {
		String result = "";
		initWithAllArtworks();
		ArrayList<DemandeArtwork> list_artwork = new ArrayList<DemandeArtwork>();
		for(int i=0; i<real_artworks.size(); i++) {
			int id = real_artworks.get(i).getIdFilm();
			boolean exclu = real_artworks.get(i).isExclu();
			list_artwork.add(new DemandeArtwork(id,exclu));
		}
		for(int j=0; j<list_artwork.size(); j++) {
			result += list_artwork.get(j).toJsonString();
		}
		return result;
	}
	
	private void sendMsg(AID aid_distributor, String content) {
		AMSAgentDescription [] agents = null;
		ACLMessage answer = new ACLMessage(ACLMessage.INFORM);        
		answer.setContent(content);
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults ( new Long(-1) );
            agents = AMSService.search( this.getAgent(), new AMSAgentDescription (), c );
            for (int i=0; i<agents.length;i++) {
            	if(agents[i].getName().equals(aid_distributor)) {
            		System.out.println("Msg INFORM envoye au distributeur "+agents[i].getName()+", "+answer.getContent());
            		answer.addReceiver(aid_distributor);
            		myAgent.send(answer);
					this.getAgent().removeBehaviour(this);
				}
            }
        }
        catch (Exception e) { 
        	e.printStackTrace(); 
        }	
	}
	
	private void initWithAllArtworks() {
		real_artworks = co.findAllArtwork();
	}
	
	

}
