package behaviors;

import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import com.google.gson.Gson;
import core.entity.*;
import core.ontologie.DemandeAchat;
import core.ontologie.PropositionPrice;
import core.statut.DemandeStatut;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
{"priceProposal":12.0}

*/
public class NegociationBehavior extends CyclicBehaviour {

    private static final Logger LOGGER = Logger.getLogger(NegociationBehavior.class.getName());

    private Connection connection = new MariaDB();

    @Override
    public void action() {
        MessageTemplate mt1 = MessageTemplate.MatchPerformative(ACLMessage.PROPOSE);
        ACLMessage negociation = myAgent.receive(mt1);

        if (negociation != null) {
            try {
                if(isCorrect(negociation)){
                    String content = negociation.getContent();
                    String sender = negociation.getSender().getName();
                    String uuid = negociation.getConversationId();

                    LOGGER.log(Level.INFO, "Demande de negociation reçue, content=" + content + "; sender=" + sender);

                    Gson gson = new Gson();
                    PropositionPrice propositionPrice = gson.fromJson(content, PropositionPrice.class);
                    double prix = propositionPrice.getPriceProposal();

                    ArrayList<DemandeArtworks> at = new ArrayList<>();
                    at = connection.findDemande(uuid);
                    boolean accept = getPriceDemandeArtworksForList (at, prix);

                    //On met à jour la précédente proposition
                    // Si on recoit une negociation la premiere offre a forcément était refusé
                    connection.updateProposition(new Proposition(uuid, DemandeStatut.REFUSE.name()));

                    //check si les id existent sinon message d'erreur
                    if(checkExcluArtworks(at)){
                        //On insert si on accepte ou pas la nouvelle proposition
                        if (accept) {
                            connection.insertProposition(new Proposition(uuid, prix, DemandeStatut.ACCEPT.name()));
                            connection.updateStatutDemande(uuid, DemandeStatut.ACCEPT.name());

                            for (DemandeArtworks a : at){
                                int nb_vente = a.getVENDU();
                                nb_vente++;
                                int id = a.getID_ARTWORK();
                                connection.updateArtworkVendu(id,nb_vente,a.isREQ_EXCLU());
                            }

                            double c = connection.findCash();
                            double somme = c+prix;
                            connection.updateCash(somme);
                        } else {
                            connection.insertProposition(new Proposition(uuid, prix, DemandeStatut.REFUSE.name()));
                            connection.updateStatutDemande(uuid, DemandeStatut.REFUSE.name());
                        }
                        sendMsg(accept, negociation, uuid);
                    }else {
                        // La demande n'est plus valable
                        connection.updateStatutDemande(uuid, DemandeStatut.VENTE_INTERROMPUE.name());
                        connection.updateProposition(new Proposition(uuid, DemandeStatut.VENTE_INTERROMPUE.name()));
                        throw new Exception("Problème sur un oeuvre : l'oeuvre a été attribué comme exclusive");
                    }
                }else{
                    throw new Exception("Problème lors du parse de la proposition d'achat");
                }
            }catch (Exception e){
                LOGGER.log(Level.SEVERE, "Envoie d'un message d'erreur aux distributeur");
                sendMsgError(negociation);
            }
        } else {
            LOGGER.log(Level.INFO, "NegociationBehaviour en arrière plan");
            this.block();
        }
    }

    /**
     * Envoie un message message d'erreur au distributeur
     * @param demande_achat
     */
    private void sendMsgError(ACLMessage demande_achat) {
        try {
            ACLMessage replyMessage = demande_achat.createReply();
            replyMessage.setContent("{\"failure\":\"Problème dans la requête\"}");
            replyMessage.setPerformative(ACLMessage.FAILURE);
            myAgent.send(replyMessage);
            LOGGER.log(Level.INFO, "Message d'erreur envoyé au distributeur !");
        }catch (Exception e){
            LOGGER.log(Level.SEVERE, "Problème lors de l'envoie du message d'erreur au distributeur !");
        }
    }

    /**
     * Envoie un message de réponse à la negociation du distributeur
     * @param accept
     * @param negociation
     * @param uuid
     */
    private void sendMsg(boolean accept, ACLMessage negociation, String uuid) {
        try {
            ACLMessage replyMessage = negociation.createReply();
            replyMessage.setConversationId(uuid);
            replyMessage.setContent("{\"replyProposal\":"+accept+"}");

            if (accept) {
                replyMessage.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
            } else {
                replyMessage.setPerformative(ACLMessage.REJECT_PROPOSAL);
            }
            myAgent.send(replyMessage);
            LOGGER.log(Level.INFO, "Réponse de la négociation envoyée !");
        }catch (Exception e){
            LOGGER.log(Level.SEVERE, "Problème envoie de la réponse de demande d'achat envoyée !");
        }
    }

    /**
     * On vérifie qu'une oeuvre n'a pas été attribuée entre la demande d'achat et la négociation
     * @param at
     * @return
     */
    private boolean checkExcluArtworks(ArrayList<DemandeArtworks> at){
        for(int i = 0; i<at.size(); i++) {
            int id_artwork = at.get(i).getID_ARTWORK();
            Artwork a = connection.findArtworkByIdFilm(id_artwork);
            // Dans la demande, le distributeur demande l'exclusivité mais on a deja vendu des oeuvres
            if(at.get(i).isREQ_EXCLU() && a.getVendu()>0){
                return false;
            }
            // Si l'oeuvre demandé est déjà exclusive, on refuse
            if(a.isExclu()){
                return false;
            }
            // verifie si on a bien produit l'oeuvre
            if(!a.isProduit()){
                return false;
            }
        }
        return true;
    }

    // Si le prix proposé est > 0.8 * le prix que nous vouliuons nous acceptons. Sinon non.
    private boolean getPriceDemandeArtworksForList (ArrayList<DemandeArtworks> listDemandeArtworks, double demandePrix) {
        double price=0.0;
        for (DemandeArtworks da : listDemandeArtworks) {
            price = price + getPriceDemandeArtworksForUnique(da);
        }
        price = price*0.8;
        if (demandePrix>price) {
            return true;
        }
        return false;
    }

    private Double getPriceDemandeArtworksForUnique (DemandeArtworks da) {
        Artwork art = connection.findArtworkByIdFilm(da.getID_ARTWORK());

        if (da.isREQ_EXCLU()) {
            return art.getPrice();
        }
        return art.getPrice()*0.9;
    }

    // Fonction utilisée pour controler ------------------------------------------
    private boolean isCorrect(ACLMessage reponse) {
        String schemaPriceProposal = "{\"$schema\": \"http://json-schema.org/draft-04/schema#\",\"title\":\"PriceProposal\",\"description\":\"prix\",\"type\":\"object\",\"properties\":{\"priceProposal\":{\"description\":\"prix\",\"type\":\"number\"}},\"required\":[\"priceProposal\"]}";
        return validateJsonDocument(schemaPriceProposal,reponse.getContent());
    }

    public static boolean validateJsonDocument(String definition, String payload){
        try {
            JSONObject jsonSchema = new JSONObject(definition);
            JSONObject jsonData = new JSONObject(payload);
            Schema schema = SchemaLoader.load(jsonSchema);
            schema.validate(jsonData);
            return true;
        } catch(ValidationException ve) {
            System.out.println(readValidationException(ve));
            return false;
        }
    }

    public static String readValidationException(final ValidationException e)
    {
        final StringBuilder bld = new StringBuilder(e.getMessage());
        e.getCausingExceptions().stream().map(ValidationException::getMessage).forEach(err ->
        {
            bld.append(err);
            bld.append("\n");
        });
        return bld.toString();
    }

}
