package behaviors;

import java.util.ArrayList;
import agents.Producteur;
import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import core.entity.Artwork;
import core.ontologie.ListIdArtwork;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;

/*


Reponse:
{
    "listArtwork": []Long
}

INFORM il recoit rien
 */

public class RecommendationBehavior extends TickerBehaviour {

	ArrayList<Artwork> real_artworks = new ArrayList<Artwork>();
	private Connection co = new MariaDB();

	public RecommendationBehavior(Agent a, long period) {
		super(a, period);
	}

	@Override
	protected void onTick() {
		initRealArtworks();		
		sendMsg("distributeur",getAllRecommendedArtworks());
	}

	private void initRealArtworks() {
		real_artworks = co.findAllArtwork();
	}

	private String getAllRecommendedArtworks() {
		ArrayList<Long> listID = new ArrayList<Long>();
		for(int i =0; i<real_artworks.size();i++) {
			if(real_artworks.get(i).isProduit()) {
				if(real_artworks.get(i).isExclu()==false) {
					Long l = new Long(real_artworks.get(i).getIdFilm());
					listID.add(l);
				}
			}
		}		
		ListIdArtwork iaa = new ListIdArtwork(listID);
		return iaa.toJsonString();
	}

	private void sendMsg(String type, String content) {
		AID[] agent = ((Producteur) this.getAgent()).findAgentsByServiceName(type);
		for (AID aid : agent) { // trouver chaque distributeur et lui envoyer le INFORM
				ACLMessage answer = new ACLMessage(ACLMessage.INFORM);
				answer.setContent(content);
				answer.addReceiver(aid);
				myAgent.send(answer);
				System.out.println("Msg Inform de recommendations envoy� au distributeur : "+aid);
			}

	}
}