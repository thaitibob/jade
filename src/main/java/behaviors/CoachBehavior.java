package behaviors;

import bdd.ScriptArtworkDB;
import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import core.entity.Artwork;
import jade.core.behaviours.SimpleBehaviour;

import java.util.ArrayList;

public class CoachBehavior extends SimpleBehaviour  {

	ScriptArtworkDB artworkDB 		= new ScriptArtworkDB();
	private Connection connection 	= new MariaDB();

	@Override
	public void action() {
		try {
			artworkDB.makeDBPropreJDBCConnection();
			ArrayList<Artwork> artwork_list = artworkDB.scriptConvertArtworks();
			setPricetoAllArtworks(artwork_list);
			for(int i=0;i<artwork_list.size();i++) {
				this.connection.insertArtwork(artwork_list.get(i));
			}
			artworkDB.PreparedStatementBdd.close();
			artworkDB.ConnectionBDDPropre.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialise le prix de tous les artworks à 100
	 * @param artwork_list
	 * @return
	 */
	public ArrayList<Artwork> setPricetoAllArtworks(ArrayList<Artwork> artwork_list) {
		for(int i=0; i<artwork_list.size(); i++) {
			artwork_list.get(i).setPrice(100);
		}
		return artwork_list;
	}

	/**
	 * return true pour que ça s'éxecute qu'une seule fois
	 * @return
	 */
	@Override
	public boolean done() {
		return true;
	}


}
