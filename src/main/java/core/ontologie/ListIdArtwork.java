package core.ontologie;

import java.util.ArrayList;

import com.google.gson.Gson;

public class ListIdArtwork {
	ArrayList<Long> listArtwork;

	public ListIdArtwork(ArrayList<Long> listArtwork) {
		this.listArtwork = listArtwork;
	}

	public ArrayList<Long>  getlistArtwork() {
		return listArtwork;
	}

	public void setlistArtwork(ArrayList<Long> listArtwork) {
		this.listArtwork = listArtwork;
	}

	@Override
	public String toString() {
		return "IdArtworkArray [listArtwork=" + listArtwork + "]";
	}
	
	public String toJsonString() {
		Gson gson = new Gson();    
	    String json = gson.toJson(this);
		return json;
	}
	
}