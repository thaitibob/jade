package core.ontologie;

import com.google.gson.Gson;

public class PropositionPrice {
    private double priceProposal;

    public PropositionPrice(double priceProposal) {
        this.priceProposal = priceProposal;
    }

    public double getPriceProposal() {
        return priceProposal;
    }

    public void setPriceProposal(double priceProposal) {
        this.priceProposal = priceProposal;
    }

    @Override
    public String toString() {
        return "PropositionPrice{" +
                "priceProposal=" + priceProposal +
                '}';
    }

    public String toJsonString() {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }
}
