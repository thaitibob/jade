package core.ontologie;

public class Popularity {
    private int oeuvre;
    private double popularite;

    public Popularity(){

    }

    public Popularity(int idOeuvre, double popularity) {
        this.oeuvre = idOeuvre;
        this.popularite = popularity;
    }

    public int getOeuvre() {
        return oeuvre;
    }

    public void setIdOeuvre(int idOeuvre) {
        this.oeuvre = idOeuvre;
    }

    public double getPopularite() {
        return popularite;
    }

    public void setPopularity(double popularity) {
        this.popularite = popularity;
    }
}
