package core.ontologie;

import core.ontologie.DemandeArtwork;

import java.io.Serializable;
import java.util.ArrayList;

public class DemandeAchat implements Serializable {
	
	private ArrayList<DemandeArtwork> listArtwork = new ArrayList<DemandeArtwork>();

	public DemandeAchat(ArrayList<DemandeArtwork> listArtwork) {
		this.listArtwork = listArtwork;
	}

	public ArrayList<DemandeArtwork> getlistArtwork() {
		return listArtwork;
	}

	public void setlistArtwork(ArrayList<DemandeArtwork> listArtwork) {
		this.listArtwork = listArtwork;
	}
}