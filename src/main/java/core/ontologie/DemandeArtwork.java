package core.ontologie;

import com.google.gson.Gson;

import java.io.Serializable;

public class DemandeArtwork implements Serializable {
	
	private int idArtwork;
	private boolean excluArtwork;
	
	public DemandeArtwork(int idArtwork, boolean excluArtwork) {
		super();
		this.idArtwork = idArtwork;
		this.excluArtwork = excluArtwork;
	}

	public int getIdArtwork() {
		return idArtwork;
	}

	public void setIdArtwork(int idArtwork) {
		this.idArtwork = idArtwork;
	}

	public boolean isExcluArtwork() {
		return excluArtwork;
	}

	public void setExcluArtwork(boolean excluArtwork) {
		this.excluArtwork = excluArtwork;
	}

	public String toJsonString() {
		Gson gson = new Gson();    
	    String json = gson.toJson(this);
		return json;
	}	
}