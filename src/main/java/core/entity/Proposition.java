package core.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Proposition {

    private int id;
    private String id_demande;
    private String date;
    private double offre;
    private String statut;

    public Proposition(String id_demande, String statut) {
        this.id_demande = id_demande;
        this.statut = statut;
        this.date = DateCourante();
    }

    public Proposition(String id_demande, double offre, String statut) {
        this.id_demande = id_demande;
        this.offre = offre;
        this.statut = statut;
        this.date = DateCourante();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId_demande() {
        return id_demande;
    }

    public void setId_demande(String id_demande) {
        this.id_demande = id_demande;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getOffre() {
        return offre;
    }

    public void setOffre(double offre) {
        this.offre = offre;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public static String DateCourante(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }
}
