package core.entity;


public class Demande {

    private String id;
    private String adresse_agent;
    private String statut;

    public Demande(String id, String adresse_agent) {
        this.id = id;
        this.adresse_agent = adresse_agent;
    }

    public Demande(String adresse_agent) {
        this.adresse_agent = adresse_agent;
    }

    public Demande() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdresse_agent() {
        return adresse_agent;
    }

    public void setAdresse_agent(String adresse_agent) {
        this.adresse_agent = adresse_agent;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
}
