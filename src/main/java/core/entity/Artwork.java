package core.entity;

public class Artwork {

	private int id;
	private int idFilm;
	private String titre;
	private String description;
	private int dateSortie;
	private boolean exclu;
	private int vendu;
	private boolean produit;
	private double price;
	
	public Artwork(int idFilm, String titre, String description, int dateSortie, boolean exclu, int vendu, boolean produit, double price) {
		super();
		this.idFilm = idFilm;
		this.titre = titre;
		this.description = description;
		this.dateSortie = dateSortie;
		this.exclu = exclu;
		this.vendu = vendu;
		this.produit = produit;
		this.price = price;
	}

	public Artwork () {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdFilm() {
		return idFilm;
	}

	public void setIdFilm(int idFilm) {
		this.idFilm = idFilm;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDateSortie() {
		return dateSortie;
	}

	public void setDateSortie(int dateSortie) {
		this.dateSortie = dateSortie;
	}
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isExclu() {
		return exclu;
	}

	public void setExclu(boolean exclu) {
		this.exclu = exclu;
	}

	public int getVendu() {
		return vendu;
	}

	public void setVendu(int vendu) {
		this.vendu = vendu;
	}
	
	public boolean isProduit() {
		return produit;
	}

	public void setProduit(boolean produit) {
		this.produit = produit;
	}

	@Override
	public String toString() {
		return "Artwork [idFilm=" + idFilm + ", titre=" + titre + ", description=" + description + ", dateSortie="
				+ dateSortie + ", exclu=" + exclu + ", price=" + price + "]";
	}

	public boolean isVendu() {
		if (this.getVendu()>0){
			return true;
		}
		return false;
	}
}
