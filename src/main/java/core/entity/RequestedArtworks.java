package core.entity;

public class RequestedArtworks {

    private int id;
    private String id_demande;
    private int id_artwork;
    private boolean exclu;

    public RequestedArtworks(String id_demande, int id_artwork, boolean exclu) {
        this.id_demande = id_demande;
        this.id_artwork = id_artwork;
        this.exclu = exclu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId_demande() {
        return id_demande;
    }

    public void setId_demande(String id_demande) {
        this.id_demande = id_demande;
    }

    public int getId_artwork() {
        return id_artwork;
    }

    public void setId_artwork(int id_artwork) {
        this.id_artwork = id_artwork;
    }

    public boolean isExclu() {
        return exclu;
    }

    public void setExclu(boolean exclu) {
        this.exclu = exclu;
    }
}
