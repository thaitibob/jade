package core.entity;

public class DemandeArtworks {

    private String ID_DEMANDE;
    private String ADRESSE_AGENT;
    private String STATUT;
    private int REQ_ID;
    private int ID_ARTWORK;
    private boolean REQ_EXCLU;
    private double PRICE;
    private int VENDU;
    private boolean PRODUIT;
    private boolean ART_EXCLU;

    public DemandeArtworks(String ID_DEMANDE, String ADRESSE_AGENT, String STATUT, int REQ_ID, int ID_ARTWORK, boolean REQ_EXCLU, double PRICE, int VENDU, boolean PRODUIT) {
        this.ID_DEMANDE = ID_DEMANDE;
        this.ADRESSE_AGENT = ADRESSE_AGENT;
        this.STATUT = STATUT;
        this.REQ_ID = REQ_ID;
        this.ID_ARTWORK = ID_ARTWORK;
        this.REQ_EXCLU = REQ_EXCLU;
        this.PRICE = PRICE;
        this.VENDU = VENDU;
        this.PRODUIT = PRODUIT;
    }

    public String getID_DEMANDE() {
        return ID_DEMANDE;
    }

    public void setID_DEMANDE(String ID_DEMANDE) {
        this.ID_DEMANDE = ID_DEMANDE;
    }

    public String getADRESSE_AGENT() {
        return ADRESSE_AGENT;
    }

    public void setADRESSE_AGENT(String ADRESSE_AGENT) {
        this.ADRESSE_AGENT = ADRESSE_AGENT;
    }

    public String getSTATUT() {
        return STATUT;
    }

    public void setSTATUT(String STATUT) {
        this.STATUT = STATUT;
    }

    public int getREQ_ID() {
        return REQ_ID;
    }

    public void setREQ_ID(int REQ_ID) {
        this.REQ_ID = REQ_ID;
    }

    public int getID_ARTWORK() {
        return ID_ARTWORK;
    }

    public void setID_ARTWORK(int ID_ARTWORK) {
        this.ID_ARTWORK = ID_ARTWORK;
    }

    public boolean isREQ_EXCLU() {
        return REQ_EXCLU;
    }

    public void setREQ_EXCLU(boolean REQ_EXCLU) {
        this.REQ_EXCLU = REQ_EXCLU;
    }

    public double getPRICE() {
        return PRICE;
    }

    public void setPRICE(double PRICE) {
        this.PRICE = PRICE;
    }

    public int getVENDU() {
        return VENDU;
    }

    public void setVENDU(int VENDU) {
        this.VENDU = VENDU;
    }

    public boolean isPRODUIT() {
        return PRODUIT;
    }

    public void setPRODUIT(boolean PRODUIT) {
        this.PRODUIT = PRODUIT;
    }

    public boolean isART_EXCLU() {
        return ART_EXCLU;
    }

    public void setART_EXCLU(boolean ART_EXCLU) {
        this.ART_EXCLU = ART_EXCLU;
    }

    @Override
    public String toString() {
        return "DemandeArtworks{" +
                "ID_DEMANDE='" + ID_DEMANDE + '\'' +
                ", ADRESSE_AGENT='" + ADRESSE_AGENT + '\'' +
                ", STATUT='" + STATUT + '\'' +
                ", REQ_ID=" + REQ_ID +
                ", ID_ARTWORK=" + ID_ARTWORK +
                ", REQ_EXCLU=" + REQ_EXCLU +
                ", PRICE=" + PRICE +
                ", VENDU=" + VENDU +
                ", PRODUIT=" + PRODUIT +
                ", ART_EXCLU=" + ART_EXCLU +
                '}';
    }
}
