package core.statut;

public enum DemandeStatut {
    EN_COURS,
    ACCEPT,
    REFUSE,
    VENTE_INTERROMPUE
}
