package agents;

import bdd.mariaDB.Connection;
import bdd.mariaDB.MariaDB;
import behaviors.*;
import behaviors.inform.InformBehavior;
import behaviors.inform.SendingListArtworksBehavior;
import core.ontologie.DemandeAchat;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class Producteur extends Agent {

    private Connection connection = new MariaDB();

    @Override
    protected void setup() {
        DFAgentDescription agentDescription = new DFAgentDescription();
        agentDescription.setName(this.getAID());

        ServiceDescription serviceDescription = new ServiceDescription();
        serviceDescription.setType("Producteur");
        serviceDescription.setName("UniversalStudio");

        //init des artworks et du montant de cash
        this.connection.init();
        this.connection.insertCash();

        try {
            DFService.register(this, agentDescription);
        } catch (FIPAException e) {
            System.err.println(this.getLocalName() + " Impossible de creer le service. Info = " + e.getMessage());
            this.doDelete();
        }
        
        // Récupérer les infos depuis la BDD
        CoachBehavior coach = new CoachBehavior();
        this.addBehaviour(coach);

        // Ontologie Production
        ProductBehavior productBehavior = new ProductBehavior(this,5000);
        this.addBehaviour(productBehavior);

        // Ontologie Demande d'achat
        DemandeAchatBehavior demandeAchatBehavior = new DemandeAchatBehavior();
        this.addBehaviour(demandeAchatBehavior);

        // ontologie Acceptation
        AcceptationBehavior accept = new AcceptationBehavior();
        this.addBehaviour(accept);

        // ontologie reputation
        ReputationBehavior repu = new ReputationBehavior(this, 60000);
        this.addBehaviour(repu);

        //{"listArtwork":["1","2"]}

        // ontologie negotiation
        NegociationBehavior negotiation = new NegociationBehavior();
        this.addBehaviour(negotiation);

        // ontologie informBehavior
        InformBehavior informBehavior = new InformBehavior();
        this.addBehaviour(informBehavior);
	
        // ontologie Recommendation
        RecommendationBehavior r = new RecommendationBehavior(this, 10000);
        this.addBehaviour(r);
    }

    /**
     * Permet de trouver tous les agents en fonction d'un service
     * @param service
     * @return tableau d'adresses d'agents
     */
    public AID[] findAgentsByServiceName(String service) {
        DFAgentDescription dfAgentDescription = new DFAgentDescription();
        ServiceDescription serviceDescription = new ServiceDescription();
        serviceDescription.setType(service);
        dfAgentDescription.addServices(serviceDescription);
        SearchConstraints searchConstraints = new SearchConstraints();
        searchConstraints.setMaxResults(new Long(-1));
        AID[] agents = null;
        try {
            DFAgentDescription[] result = DFService.search(this, dfAgentDescription, searchConstraints);
            agents = new AID[result.length];
            
            for (int i = 0; i < result.length; i++) {
                agents[i] = result[i].getName();
            }
            
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        return agents;
    }

	

}
