package bdd.mariaDB;

import core.statut.DemandeStatut;
import core.entity.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MariaDB implements Connection{

    private static final Logger LOGGER = Logger.getLogger(MariaDB.class.getName());

    private java.sql.Connection connect;
    private final String driverName = "org.mariadb.jdbc.Driver";
    private final String jdbc = "jdbc:mariadb://";
    private final String host = "localhost:";
    private final String port = "3309/";
    private final String database = "agt_producteur";
    private final String url = jdbc + host + port + database;
    private final String username = "?user=root";
    private final String password = "&password=admin";
    private final String variables = "&createDatabaseIfNotExist=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String connection = url + username + password + variables;

    /**
     * Connexion à la BDD
     */
    public MariaDB() {
        if (connect == null) {
            try {
                Class.forName(driverName);
                LOGGER.log(Level.INFO, "Driver Class MariaDB Found");
                try {
                    connect = DriverManager.getConnection(connection);
                    LOGGER.log(Level.INFO, "Connection réussie");
                } catch (SQLException e) {
                    LOGGER.log(Level.SEVERE, "Connection échouée : "+ e);
                }
            } catch (ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Driver Class MariaDB not found : "+ e);
            }
        }
    }

    /**
     * Initialisation de la BDD
     */
    @Override
    public void init() {
        try {
            Statement stmt;
            String sql = "DROP TABLE IF EXISTS `demande`;";
            stmt = connect.createStatement();
            stmt.executeUpdate(sql);

            sql = "DROP TABLE IF EXISTS `artwork`;";
            stmt = connect.createStatement();
            stmt.executeUpdate(sql);

            sql = "DROP TABLE IF EXISTS `requested_artworks`;";
            stmt = connect.createStatement();
            stmt.executeUpdate(sql);

            sql = "DROP TABLE IF EXISTS `proposition`;";
            stmt = connect.createStatement();
            stmt.executeUpdate(sql);

            sql = "DROP TABLE IF EXISTS `cash`;";
            stmt = connect.createStatement();
            stmt.executeUpdate(sql);

            // creation de la table demande
            sql = "create table IF NOT EXISTS `demande`\n" +
                    "(\n" +
                    "\tid text null,\n" +
                    "\tadresse_agent varchar(255) null,\n" +
                    "\tstatut varchar(255) null\n" +
                    ");";
            stmt = connect.createStatement();
            stmt.executeUpdate(sql);

            // creation de la table artwork
            sql = "CREATE TABLE IF NOT EXISTS `artwork` ( "
                    + "`id` int(11) NOT NULL AUTO_INCREMENT,"
                    + "`idFilm` int(11) NOT NULL,"
                    + "`titre` varchar(250) DEFAULT NULL,"
                    + "`description` text DEFAULT NULL,"
                    + "`datedesortie` int(11) NOT NULL,"
                    + "`exclu` bool NOT NULL,"
                    + "`vendu` int(11) NOT NULL,"
                    + "`price` double DEFAULT NULL,"
                    + "`produit` bool DEFAULT NULL,"
                    + "PRIMARY KEY (`id`)"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";
            stmt = connect.createStatement();
            stmt.executeUpdate(sql);

            // creation de la cash
            sql = "CREATE TABLE IF NOT EXISTS `cash` ( "
                    + "`id` int(11) NOT NULL AUTO_INCREMENT,"
                    + "`montant` int(11) NOT NULL,"
                    + "PRIMARY KEY (`id`)"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";
            stmt = connect.createStatement();
            stmt.executeUpdate(sql);

            // creation de la table proposition
            sql = "CREATE TABLE IF NOT EXISTS `proposition` ( "
                    + "`id` int(11) NOT NULL AUTO_INCREMENT,"
                    + "`id_demande` text DEFAULT NULL,"
                    + "`date` varchar(50) DEFAULT NULL,"
                    + "`offre` double DEFAULT NULL,"
                    + "`statut` varchar(50) DEFAULT NULL,"
                    + "PRIMARY KEY (`id`)"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";
            stmt = connect.createStatement();
            stmt.executeUpdate(sql);

            // creation de la table requested_artworks
            sql = "CREATE TABLE IF NOT EXISTS `requested_artworks` ( "
                    + "`id` int(11) NOT NULL AUTO_INCREMENT,"
                    + "`id_demande` text DEFAULT NULL,"
                    + "`id_artwork` int(11) DEFAULT NULL,"
                    + "`exclu` bool DEFAULT NULL,"
                    + "PRIMARY KEY (`id`)"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";
            stmt = connect.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            LOGGER.log(Level.INFO, "Script d'initialisation MariaDB réussie");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème Script d'initialisation MariaDB : "+ e);
        }
    }

    /**
     * Insertion du cash par défaut
     */
    @Override
    public void insertCash() {
        try {
            String insertCash = "INSERT INTO cash(montant) VALUES (1000)";
            PreparedStatement InsertCash = null;
            InsertCash = connect.prepareStatement(insertCash);
            InsertCash.executeUpdate();
            InsertCash.close();
            LOGGER.log(Level.INFO, "Insertion du cash MariaDB réussie");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème Insertion du cash MariaDB : "+ e);
        }
    }

    /**
     * met à jour notre ptf
     * @param montant montant à mettre à jour
     */
    @Override
    public void updateCash(double montant) {
        try {
            String updateCash = "UPDATE cash SET montant = ?";
            PreparedStatement UpdateCash = null;
            UpdateCash = connect.prepareStatement(updateCash);
            UpdateCash.setDouble(1, montant);
            UpdateCash.executeUpdate();
            UpdateCash.close();
            LOGGER.log(Level.INFO, "update du cash MariaDB réussie");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème update du cash MariaDB : "+ e);
        }
    }

    /**
     * met à jour notre ptf
     */
    @Override
    public double findCash() {
        try {
            PreparedStatement stmt = null;
            stmt = connect.prepareStatement("SELECT * FROM cash");
            ResultSet rs = stmt.executeQuery();
            double montant = 0.0;
            while ( rs.next() ) {
                montant = rs.getDouble("montant");
            }
            LOGGER.log(Level.INFO, "Récupération du cash MariaDB réussie");
            return montant;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème récupération du cash MariaDB : "+ e);
        }
        return 0.0;
    }

    /**
     * Insertion d'une demande dans la BDD
     * @param d
     */
    @Override
    public void insertDemande(Demande d) {
        try {
            String insertOeuvre = "INSERT INTO demande(id,adresse_agent,statut) VALUES (?,?,?)";
            PreparedStatement InsertDemande = null;
            InsertDemande = connect.prepareStatement(insertOeuvre);
            InsertDemande.setString(1, d.getId());
            InsertDemande.setString(2, d.getAdresse_agent());
            InsertDemande.setString(3, DemandeStatut.EN_COURS.name());
            InsertDemande.executeUpdate();
            InsertDemande.close();
            LOGGER.log(Level.INFO, "Insertion Demande MariaDB réussie");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème Insertion Demande MariaDB : "+ e);
        }
    }

    /**
     * Insertion d'un artwork dans la BDD
     * @param a
     */
    @Override
    public void insertArtwork(Artwork a) {
        try {
            String insertArtwork = "INSERT INTO artwork(idFilm,titre,description,datedesortie,exclu,vendu,price,produit) VALUES(?,?,?,?,?,?,?,?)";
            PreparedStatement InsertArtwork = null;
            InsertArtwork = connect.prepareStatement(insertArtwork);
            InsertArtwork.setInt(1, a.getIdFilm());
            InsertArtwork.setString(2, a.getTitre());
            InsertArtwork.setString(3, a.getDescription());
            InsertArtwork.setInt(4, a.getDateSortie());
            InsertArtwork.setBoolean(5, a.isExclu());
            InsertArtwork.setInt(6, 0);
            InsertArtwork.setDouble(7, a.getPrice());
            InsertArtwork.setBoolean(8, a.isProduit());
            InsertArtwork.executeUpdate();
            InsertArtwork.close();
            LOGGER.log(Level.INFO, "Insertion Artwork MariaDB réussie");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème Insertion Artwork MariaDB : "+ e);
        }
    }

    /**
     * Insertion d'une proposition
     * @param p proposition
     */
    @Override
    public void insertProposition(Proposition p) {
        try {
            String insertArtwork = "INSERT INTO proposition(id_demande,date,offre,statut) VALUES(?,?,?,?)";
            PreparedStatement InsertProposition = null;
            InsertProposition = connect.prepareStatement(insertArtwork);
            InsertProposition.setString(1, p.getId_demande());
            InsertProposition.setString(2, p.getDate());
            InsertProposition.setDouble(3, p.getOffre());
            InsertProposition.setString(4, p.getStatut());
            InsertProposition.executeUpdate();
            InsertProposition.close();
            LOGGER.log(Level.INFO, "Insertion Proposition MariaDB réussie");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème Insertion Proposition MariaDB : "+ e);
        }
    }

    /**
     * Vérifie que l'agent ne possède pas l'oeuvre
     * @param agent
     * @param idFilm
     * @return
     */
    @Override
    public boolean DistributeurisNotOwner(String agent, int idFilm) {
        int count = 0;
        try {
            PreparedStatement stmt = null;
            stmt = connect.prepareStatement("select *\n" +
                    "from demande\n" +
                    "         inner join requested_artworks on requested_artworks.id_demande = demande.id\n" +
                    "where demande.statut = ?\n" +
                    "  and requested_artworks.id_artwork = ?\n" +
                    "  and demande.adresse_agent = ?");
            stmt.setString(1, DemandeStatut.ACCEPT.name());
            stmt.setInt(1, idFilm);
            stmt.setString(1, agent);
            ResultSet rs = stmt.executeQuery();

            while ( rs.next() ) {
                count++;
            }
            LOGGER.log(Level.INFO, "Récupération du cash MariaDB réussie");
            return false;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème récupération du cash MariaDB : "+ e);
        }
        return count == 0 ? true : false;
    }

    /**
     * Insertion d'un artwork lié à une demande (une demande peut avoir plusieurs artwork)
     * @param r
     */
    @Override
    public void insertRequested_artworks(RequestedArtworks r) {
        try {
            String insertReq_Artwork = "INSERT INTO requested_artworks(id_demande,id_artwork,exclu) VALUES (?,?,?)";
            PreparedStatement InsertReq_Artwork = null;
            InsertReq_Artwork = connect.prepareStatement(insertReq_Artwork);
            InsertReq_Artwork.setString(1, r.getId_demande());
            InsertReq_Artwork.setInt(2, r.getId_artwork());
            InsertReq_Artwork.setBoolean(3, r.isExclu());
            InsertReq_Artwork.executeUpdate();
            InsertReq_Artwork.close();
            LOGGER.log(Level.INFO, "Insertion RequestedArtworks MariaDB réussie");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème Insertion RequestedArtworks MariaDB : "+ e);
        }
    }

    /**
     * Maj du prix artwork
     * @param artwork
     */
    @Override
    public void updateArtwork(Artwork artwork) {
        PreparedStatement stmt = null;
        try {
            stmt = connect.prepareStatement("UPDATE artwork set price=?, produit=? where idFilm=?");

            stmt.setDouble(1, artwork.getPrice());
            stmt.setBoolean(2, artwork.isProduit());
            stmt.setInt(3, artwork.getIdFilm());

            stmt.executeUpdate();
            LOGGER.log(Level.INFO, "MAJ artwork MariaDB réussie");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème MAJ artwork MariaDB : "+ e);
        }
    }

    @Override
    public void updateArtworkVendu(int id, int vendu, boolean exclu) {
        PreparedStatement stmt = null;
        try {
            stmt = connect.prepareStatement("UPDATE artwork set exclu=?, vendu=? where idFilm=?");

            stmt.setBoolean(1, exclu);
            stmt.setInt(2, vendu);
            stmt.setInt(3, id);

            stmt.executeUpdate();
            LOGGER.log(Level.INFO, "MAJ updateArtworkVendu MariaDB réussie");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème MAJ updateArtworkVendu MariaDB : "+ e);
        }
    }

    @Override
    public double getPropositionPrice(Proposition p) {
        try {
            PreparedStatement stmt = null;
            stmt = connect.prepareStatement("SELECT * FROM proposition where statut=? and id_demande=?");
            stmt.setString(1, DemandeStatut.EN_COURS.name());
            stmt.setString(2, p.getId_demande());
            ResultSet rs = stmt.executeQuery();
            double offre = 0.0;
            while ( rs.next() ) {
                offre = rs.getDouble("offre");
            }
            LOGGER.log(Level.INFO, "Récupération du cash MariaDB réussie");
            return offre;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème récupération du cash MariaDB : "+ e);
        }
        return 0.0;
    }

    /**
     * Maj du statut d'une proposition
     * @param p Proposition
     */
    @Override
    public void updateProposition(Proposition p) {

        PreparedStatement stmt = null;
        try {
            stmt = connect.prepareStatement("UPDATE proposition set statut=? , date=? WHERE id_demande=? AND statut=?");

            stmt.setString(1, p.getStatut());
            stmt.setString(2, p.getDate());
            stmt.setString(3, p.getId_demande());
            stmt.setString(4, DemandeStatut.EN_COURS.name());

            stmt.executeUpdate();
            LOGGER.log(Level.INFO, "MAJ proposition MariaDB réussie");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème MAJ proposition MariaDB : "+ e);
        }
    }

    /**
     * Met à jour le statut d'une demande
     * @param i
     */
    @Override
    public void updateStatutDemande(String i, String statut) {
        PreparedStatement stmt = null;
        try {
            stmt = connect.prepareStatement("UPDATE demande set statut=? where id=?");

            stmt.setString(1, statut);
            stmt.setString(2, i);

            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Cherche un artwork par idFilm et retourne l'objet
     * @param idFilm
     * @return
     */
    @Override
    public Artwork findArtworkByIdFilm(int idFilm) {
        try {
            PreparedStatement stmt = null;
            ArrayList<Demande> matches = new ArrayList<Demande>();

            stmt = connect.prepareStatement("SELECT * FROM artwork where idFilm=?");
            stmt.setInt(1, idFilm);

            ResultSet rs = stmt.executeQuery();

            Artwork artwork = null;
            while ( rs.next() ) {
                artwork = new Artwork();
                artwork = setArtwork(rs, artwork);
            }
            LOGGER.log(Level.INFO, "Recuperation de l'artwork "+idFilm+" MariaDB réussie");
            return artwork;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème Recuperation de l'artwork "+idFilm+" : "+ e);
        }
        return null;
    }

    /**
     * Récupère tous les artworks
     * @return
     */
    @Override
    public ArrayList<Artwork> findAllArtwork() {
        PreparedStatement stmt = null;
        try {
            stmt = connect.prepareStatement("SELECT * FROM artwork");

            ResultSet rs = stmt.executeQuery();

            ArrayList<Artwork> matches = new ArrayList<Artwork>();
            while ( rs.next() ) {
                Artwork artwork = new Artwork();
                artwork = setArtwork(rs, artwork);
                matches.add(artwork);
                //LOGGER.log(Level.INFO, "Récupération ALL Artwork MariaDB réussie");
            }
            return matches;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème Récupération ALL Artwork MariaDB : "+ e);
        }
        return null;
    }


    /**
     * Retourne la liste des toutes les demandes avec toutes les informations
     * @param id_demande
     * @return liste de DemandeArtworks
     */
    @Override
    public ArrayList<DemandeArtworks> findDemande(String id_demande) {

        PreparedStatement stmt = null;
        try {
            ArrayList<DemandeArtworks> matches = new ArrayList<DemandeArtworks>();
            String getDemandeArtWork = "SELECT\n" +
                    "    DEM.id as ID_DEMANDE\n" +
                    "    , DEM.adresse_agent AS ADRESSE_AGENT\n" +
                    "    , DEM.statut AS STATUT\n" +
                    "    , REQ.id AS REQ_ID\n" +
                    "    , REQ.id_artwork AS ID_ARTWORK\n" +
                    "    , REQ.exclu AS REQ_EXCLU\n" +
                    "    , ART.price AS PRICE\n" +
                    "    , ART.vendu AS VENDU\n" +
                    "    , ART.produit as PRODUIT\n" +
                    "    , ART.exclu as ART_EXCLU\n" +
                    "FROM demande DEM\n" +
                    "INNER JOIN requested_artworks REQ\n" +
                    "    ON REQ.id_demande = DEM.id\n" +
                    "INNER JOIN artwork ART\n" +
                    "    ON ART.idFilm = REQ.id_artwork\n" +
                    "WHERE\n" +
                    "    DEM.id = ?\n";

            stmt = connect.prepareStatement(getDemandeArtWork);
            stmt.setString(1, id_demande);

            ResultSet rs = stmt.executeQuery();

            while ( rs.next() ) {
                DemandeArtworks d = new DemandeArtworks(
                        rs.getString("ID_DEMANDE"),
                        rs.getString("ADRESSE_AGENT"),
                        rs.getString("STATUT"),
                        rs.getInt("REQ_ID"),
                        rs.getInt("ID_ARTWORK"),
                        rs.getBoolean("REQ_EXCLU"),
                        rs.getInt("PRICE"),
                        rs.getInt("VENDU"),
                        rs.getBoolean("PRODUIT")
                );
                matches.add(d);
            }
            //LOGGER.log(Level.INFO, "Récupération DemandeArtworks MariaDB réussie");
            return matches;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problème Récupération Artwork MariaDB : "+ e);
        }
        return null;
    }

    /**
     * Cherche les artworks non produits
     * @return
     */
    @Override
    public ArrayList<Artwork> findAllArtworkNotProduced() {
        PreparedStatement stmt = null;
        try {
            stmt = connect.prepareStatement("SELECT * FROM artwork WHERE produit=0");

            ResultSet rs = stmt.executeQuery();

            ArrayList<Artwork> matches = new ArrayList<Artwork>();
            while ( rs.next() ) {
                Artwork artwork = new Artwork();
                artwork = setArtwork(rs, artwork);
                matches.add(artwork);
            }
            return matches;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<Artwork>();
    }

    /**
     * Met a jour le prix d'un artwork
     * @param idOeuvre
     * @param prix
     */
    @Override
    public void updateArtworkPrice(int idOeuvre, double prix) {

        PreparedStatement stmt = null;
        try {
            stmt = connect.prepareStatement("UPDATE artwork set price=? where idFilm=?");

            stmt.setDouble(1, prix);
            stmt.setInt(2, idOeuvre);

            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Transpose un artwork sql en objet artwork java
     * @param rs
     * @param artwork
     * @return
     * @throws SQLException
     */
    private Artwork setArtwork(ResultSet rs, Artwork artwork) throws SQLException {
        artwork.setId(rs.getInt("id"));
        artwork.setIdFilm(rs.getInt("idFilm"));
        artwork.setTitre(rs.getString("titre"));
        artwork.setDescription(rs.getString("description"));
        artwork.setDateSortie(rs.getInt("datedesortie"));
        artwork.setExclu(rs.getBoolean("exclu"));
        artwork.setVendu(rs.getInt("vendu"));
        artwork.setPrice(rs.getDouble("price"));
        artwork.setProduit(rs.getBoolean("produit"));
        return artwork;
    }

}