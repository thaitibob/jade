package bdd.mariaDB;

import core.entity.*;

import java.util.ArrayList;
import java.util.List;

public interface Connection {

    public void init();

    public double findCash();
    public void updateCash(double montant);
    public void insertCash();
    public void insertDemande(Demande d);
    public void insertArtwork(Artwork a);
    public void insertRequested_artworks(RequestedArtworks r);
    public void updateArtworkPrice(int idOeuvre, double prix);
    public void updateArtwork(Artwork artwork);
    public void updateArtworkVendu(int id, int vendu, boolean exclu);
    public void updateStatutDemande(String i, String statut);
    public void updateProposition(Proposition p);
    public double getPropositionPrice(Proposition p);
    public void insertProposition(Proposition p);
    public boolean DistributeurisNotOwner(String agent, int idFilm);
    public ArrayList<Artwork> findAllArtwork();
    public ArrayList<Artwork> findAllArtworkNotProduced();
    public Artwork findArtworkByIdFilm(int id);
    ArrayList<DemandeArtworks> findDemande(String i);



}
