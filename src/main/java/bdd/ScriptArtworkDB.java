package bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

import bdd.mariaDB.MariaDB;
import core.entity.Artwork;

public class ScriptArtworkDB {

	private static final Logger LOGGER = Logger.getLogger(ScriptArtworkDB.class.getName());
	
	public Connection ConnectionBDDPropre = null;
	public PreparedStatement PreparedStatementBdd = null;

	public void makeDBPropreJDBCConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			log("Congrats - Seems your MySQL JDBC Driver Registered!");
		} catch (ClassNotFoundException e) {
			log("Sorry, couldn't find JDBC driver. Make sure you have added JDBC Maven Dependency Correctly");
			e.printStackTrace();
			return;
		}
		try {
			ConnectionBDDPropre = DriverManager.getConnection("jdbc:mysql://mysql-agentmiage2021.alwaysdata.net/agentmiage2021_dbpropre", "227750", "mdpUser");
			if (ConnectionBDDPropre != null) {
				log("Connection Successful! Enjoy. Now it's time to push data");
			} else {
				log("Failed to make connection!");
			}
		} catch (SQLException e) {
			log("MySQL Connection Failed!");
			e.printStackTrace();
			return;
		}
	}
	
	public ArrayList<Artwork> scriptConvertArtworks() {
		ArrayList<Artwork> result = new ArrayList<Artwork>();
		try {
			String getOeuvres = "SELECT * FROM oeuvre";
			PreparedStatementBdd = ConnectionBDDPropre.prepareStatement(getOeuvres);		
			ResultSet rs = PreparedStatementBdd.executeQuery();
			while (rs.next()) {
				int idFilm = Integer.parseInt(rs.getString("id"));
				String titre = rs.getString("titre");
				String description = rs.getString("description");
				int dateSortie = rs.getInt("dateSortie");
				Artwork a = new Artwork(idFilm,titre,description,dateSortie,false,0,false,0);
				result.add(a);
			}
			Artwork a1 = new Artwork(1,"Au revoir là haut","un gueule-cassé de la première guerre mondiale se met à la peinture",2017,false,0,true,0);
			Artwork a2 = new Artwork(2,"Independance Day","Will Smith est de retour pour botter le fondement des extraterrestres",2016,false,0,true,0);
			Artwork a3 = new Artwork(3,"Miss","Un film joyeux qui parle du monde du mannequinat",2020,false,0,true,0);
			Artwork a4 = new Artwork(4,"Peninsula","Il y a quatre ans, la Corée a été la victime une attaque de zombies meurtrière. Peu d'habitants ont survécu. Jung-seok, un ancien soldat qui a réussi à quitter le pays, doit revenir dans une zone infestée de mort-vivants.",2020,false,0,true,0);
			Artwork a5 = new Artwork(5,"Le bon la brute et le cinglé","Les années 30 en Mandchourie. Le Cinglé vole une carte aux trésors à un haut dignitaire japonais. La Brute, tueur à gages réputé, est payé pour récupérer cette carte. Le Bon veut retrouver le détenteur de la carte pour empocher la prime. ",2008,false,0,true,0);
			result.add(a1);
			result.add(a2);
			result.add(a3);
			result.add(a4);
			result.add(a5);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return result;
	}

	// Simple log utility
	private  void log(String string) {
		System.out.println(string);
	}
}